require 'test_helper'

class BillControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get bill_index_url
    assert_response :success
  end

  test "should get show" do
    get bill_show_url
    assert_response :success
  end

  test "should get new" do
    get bill_new_url
    assert_response :success
  end

  test "should get create" do
    get bill_create_url
    assert_response :success
  end

  test "should get update" do
    get bill_update_url
    assert_response :success
  end

  test "should get destroy" do
    get bill_destroy_url
    assert_response :success
  end

end
