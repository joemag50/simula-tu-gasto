class Bill < ApplicationRecord
  attr_accessor :mes, :tarifa, :kwh_anterior, :kwh_actual,
                :metros_cubicos_anterior, :metros_cubicos_actual
  belongs_to :user
  enum bill_type: [:luz, :agua, :gas]
  has_many :bill_payments
  after_save :send_mail

  TARIFA1 = {
    enero: {
      basico: 0.796,
      intermedio: 0.960,
      excedente: 2.813
    },
    febrero: {
      basico: 0.799,
      intermedio: 0.964,
      excedente: 2.824
    },
    marzo: {
      basico: 0.802,
      intermedio: 0.968,
      excedente: 2.835
    },
    abril: {
      basico: 0.805,
      intermedio: 0.972,
      excedente: 2.846
    },
    mayo: {
      basico: 0.808,
      intermedio: 0.976,
      excedente: 2.857
    },
    junio: {
      basico: 0.811,
      intermedio: 0.980,
      excedente: 2.868
    },
    julio: {
      basico: 0.814,
      intermedio: 0.984,
      excedente: 2.879
    },
    agosto: {
      basico: 0.817,
      intermedio: 0.988,
      excedente: 2.890
    },
    septiembre: {
      basico: 0.820,
      intermedio: 0.992,
      excedente: 2.901
    },
    octubre: {
      basico: 0.823,
      intermedio: 0.996,
      excedente: 2.912
    },
    noviembre: {
      basico: 0.826,
      intermedio: 1.000,
      excedente: 2.923
    },
    diciembre: {
      basico: 0.829,
      intermedio: 1.004,
      excedente: 2.934
    }
  }.freeze

  TARIFA1C = {
    enero: {
      basico: 0.796,
      intermedio: 0.960,
      excedente: 2.813
    },
    febrero: {
      basico: 0.799,
      intermedio: 0.964,
      excedente: 2.824
    },
    marzo: {
      basico: 0.802,
      intermedio: 0.968,
      excedente: 2.835
    },
    abril: {
      basico: 0.709,
      intermedio_bajo: 0.834,
      intermedio_alto: 1.066,
      excedente: 2.846
    },
    mayo: {
      basico: 0.712,
      intermedio_bajo: 0.837,
      intermedio_alto: 1.070,
      excedente: 2.857
    },
    junio: {
      basico: 0.715,
      intermedio_bajo: 0.840,
      intermedio_alto: 1.074,
      excedente: 2.868
    },
    julio: {
      basico: 0.718,
      intermedio_bajo: 0.843,
      intermedio_alto: 1.078,
      excedente: 2.879
    },
    agosto: {
      basico: 0.721,
      intermedio_bajo: 0.846,
      intermedio_alto: 1.082,
      excedente: 2.890
    },
    septiembre: {
      basico: 0.724,
      intermedio_bajo: 0.849,
      intermedio_alto: 1.086,
      excedente: 2.901
    },
    octubre: {
      basico: 0.823,
      intermedio: 0.996,
      excedente: 2.912
    },
    noviembre: {
      basico: 0.826,
      intermedio: 1.000,
      excedente: 2.923
    },
    diciembre: {
      basico: 0.829,
      intermedio: 1.004,
      excedente: 2.934
    }
  }.freeze

  AGUA2 = [
  0,
  1.6,
  3.21,
  4.81,
  6.41,
  8.02,
  9.63,
  24.02,
  36.65,
  49.23,
  61.78,
  85.96,
  100.55,
  115.11,
  129.66,
  144.25,
  155.82,
  167.58,
  179.35,
  191.02,
  202.75,
  227.22,
  251.68,
  276.03,
  300.57,
  325.16,
  351.2,
  377.53,
  403.76,
  429.93,
  456.15,
  482.98,
  509.76,
  536.54,
  563.25,
  590.01,
  619.38,
  648.47,
  677.59,
  706.88,
  736.3,
  767.88,
  799.53,
  831.51,
  863.11,
  895.11,
  929.38,
  963.74,
  998.22,
  1032.8,
  1067.19,
  1103.32,
  1139.58,
  1175.34,
  1211.29,
  1247.4,
  1284.92,
  1322.76,
  1360.03,
  1398,
  1435.58,
  1474.98,
  1514.04,
  1553.35,
  1592.96,
  1631.99,
  1671.36,
  1710.43,
  1749.55,
  1789,
  1828.4,
  1869.43,
  1910.16,
  1950.84,
  1991.9,
  2032.69,
  2073.28,
  2114.61,
  2155.24,
  2196.6,
  2237.4,
  2279.44,
  2322.34,
  2364.99,
  2407.96,
  2450.66,
  2493.57,
  2536.05,
  2578.95,
  2621.5,
  2664.45,
  2709.07,
  2753.33,
  2798.25,
  2842.44,
  2886.81,
  2932.08,
  2976.09,
  3021.1,
  3065.29,
  3109.63,
  3156.5,
  3202.93,
  3249.52,
  3295.63,
  3342.29,
  3388.95,
  3435.64,
  3482.05,
  3528.2,
  3574.45,
  3663.41,
  3751.86,
  3840.87,
  3928.86,
  4017.81,
  4106.55,
  4195.04,
  4283.1,
  4372.51,
  4460.55,
  4551.41,
  4642.1,
  4732.43,
  4822.98,
  4913.29,
  5003.46,
  5094.45,
  5184.64,
  5275.14,
  5365.59,
  5458.77,
  5549.94,
  5642.69,
  5734.36,
  5827.79,
  5920.42,
  6012.59,
  6104.43,
  6196.96,
  6288.8,
  6383.43,
  6477.33,
  6572.64,
  6666.66,
  6760.45,
  6854.7,
  6948.51,
  7043.14,
  7137.29,
  7231.5,
  7327.84,
  7424.44,
  7520.14,
  7615.88,
  7712.41,
  7808.58,
  7903.97,
  8000.93,
  8097.28,
  8192.2,
  8290.7,
  8389.78,
  8487,
  8585.81,
  8683.95,
  8781.61,
  8880.23,
  8978.76,
  9076.56,
  9175.54,
  9274.61,
  9375.29,
  9475.64,
  9576.09,
  9676.63,
  9776.14,
  9877.71,
  9977.79,
  10077.48,
  10178.33,
  10279.44,
  10382.88,
  10486.13,
  10587.82,
  10690.69,
  10793.35,
  10895.55,
  10997.3,
  11100.91,
  11203.11,
  11307.91,
  11412.24,
  11517.04,
  11621.33,
  11725.84,
  11831.05,
  11935.25,
  12039.39,
  12144.23,
  12205.25]

  AGUA1 = [
    0,
    0.64,
    1.28,
    1.92,
    2.56,
    3.21,
    3.85,
    5.37,
    6.14,
    6.9,
    7.67,
    20.26,
    31.7,
    43.13,
    54.61,
    66.05,
    78.08,
    90.1,
    102.17,
    114.17,
    126.24,
    140.09,
    153.89,
    167.62,
    181.44,
    195.35,
    214.37,
    233.5,
    252.64,
    271.67,
    290.85,
    311.64,
    332.35,
    353.07,
    373.93,
    394.38,
    417.13,
    439.67,
    462.35,
    485.04,
    507.44,
    532.1,
    556.67,
    581.27,
    605.79,
    630.14,
    656.88,
    683.71,
    710.35,
    736.91,
    763.7,
    791.52,
    819.42,
    847.21,
    875.23,
    902.99,
    932.12,
    961.25,
    990.35,
    1019.64,
    1048.86,
    1079.15,
    1109.55,
    1139.67,
    1170.24,
    1200.55,
    1231.03,
    1261.21,
    1291.52,
    1321.83,
    1352.26,
    1383.93,
    1415.95,
    1447.3,
    1478.82,
    1510.65,
    1542.19,
    1574.19,
    1605.32,
    1637.2,
    1668.88,
    1702.05,
    1734.96,
    1768.23,
    1801.3,
    1834.13,
    1866.72,
    1900.17,
    1933.27,
    1966.19,
    1999.26,
    2033.58,
    2068.34,
    2102.73,
    2137.18,
    2172.18,
    2206.18,
    2240.7,
    2275.17,
    2309.67,
    2344.5,
    2380.07,
    2416.07,
    2452.12,
    2488.1,
    2524.31,
    2559.79,
    2596.03,
    2631.64,
    2667.45,
    2703.58,
    2772.56,
    2840.77,
    2909.64,
    2977.79,
    3046.47,
    3114.72,
    3183.34,
    3252.55,
    3320.58,
    3389.4,
    3459.27,
    3529.22,
    3599.47,
    3669.41,
    3739,
    3809.11,
    3878.83,
    3949.31,
    4019.51,
    4089.67,
    4160.69,
    4231.79,
    4304.01,
    4375.1,
    4446.77,
    4518.19,
    4589.91,
    4661.09,
    4732.26,
    4803.4,
    4876.91,
    4949.98,
    5022.59,
    5095.58,
    5168.24,
    5241.55,
    5314.05,
    5386.9,
    5460.11,
    5533.8,
    5607.23,
    5681.3,
    5756.17,
    5830.13,
    5905.04,
    5979.32,
    6053.29,
    6128.19,
    6202.11,
    6277.12,
    6352.74,
    6428.81,
    6504.03,
    6580.01,
    6656.76,
    6731.8,
    6808.09,
    6884.47,
    6960.43,
    7036.3,
    7113.6,
    7191.84,
    7269.11,
    7346.98,
    7424.73,
    7502.88,
    7579.67,
    7657.56,
    7734.59,
    7813.26,
    7892.32,
    7971.24,
    8051.09,
    8130.59,
    8209.56,
    8288.35,
    8367.69,
    8447.78,
    8526.92,
    8606.24,
    8686.3,
    8767.49,
    8847.7,
    8929.63,
    9010.17,
    9091.07,
    9172.12,
    9253.33,
    9334.1,
    9414.8
  ]

  GASTARIFA = {
    enero: 3.501,
    febrero: 3.113,
    marzo: 2.481,
    abril: 2.657,
    mayo: 2.740,
    junio: 2.898,
    julio: 2.706,
    agosto: 2.896,
    septiembre: 2.714,
    octubre: 2.700,
    noviembre: 3.118,
    diciembre: 3.250
  }

  def calcular_luz params
    kwh_anterior = params[:kwh_anterior].to_f
    kwh_actual = params[:kwh_actual].to_f

    kkwwhh = kwh_actual - kwh_anterior
    tmp_kwh = kkwwhh
    total = 0

    if params[:tarifa] == 'tarifa1'
      if kkwwhh <= 75.0
        nivel = :basico
        return TARIFA1[params[:mes].to_sym][nivel.to_sym] * tmp_kwh
      end

      if kkwwhh <= 215
        nivel = :basico
        total += TARIFA1[params[:mes].to_sym][nivel.to_sym] * 75.0

        tmp_kwh -= 75.0

        nivel = :intermedio
        total += TARIFA1[params[:mes].to_sym][nivel.to_sym] * tmp_kwh

        return total
      end

      if kkwwhh > 215
        nivel = :basico
        total += TARIFA1[params[:mes].to_sym][nivel.to_sym] * 75.0

        tmp_kwh -= 75.0

        nivel = :intermedio
        total += TARIFA1[params[:mes].to_sym][nivel.to_sym] * 140

        tmp_kwh -= 140

        nivel = :excedente
        total += TARIFA1[params[:mes].to_sym][nivel.to_sym] * tmp_kwh

        return total
      end
    end

    if params[:tarifa] == 'tarifa1c'
      if [:enero, :febrero, :marzo, :octubre, :noviembre, :diciembre].include? params[:mes].to_sym
        if kkwwhh <= 75.0
          nivel = :basico
          return TARIFA1C[params[:mes].to_sym][nivel.to_sym] * tmp_kwh
        end

        if kkwwhh <= 175.0
          nivel = :basico
          total += TARIFA1C[params[:mes].to_sym][nivel.to_sym] * 75.0

          tmp_kwh -= 75.0

          nivel = :intermedio
          total += TARIFA1C[params[:mes].to_sym][nivel.to_sym] * tmp_kwh

          return total
        end

        if kkwwhh > 175.0
          nivel = :basico
          total += TARIFA1C[params[:mes].to_sym][nivel.to_sym] * 75.0

          tmp_kwh -= 75.0

          nivel = :intermedio
          total += TARIFA1C[params[:mes].to_sym][nivel.to_sym] * 100.0

          tmp_kwh -= 100.0

          nivel = :excedente
          total += TARIFA1C[params[:mes].to_sym][nivel.to_sym] * tmp_kwh

          return total
        end
      else
        if kkwwhh <= 150.0
          nivel = :basico
          return TARIFA1C[params[:mes].to_sym][nivel.to_sym] * tmp_kwh
        end

        if kkwwhh <= 300.0
          nivel = :basico
          total += TARIFA1C[params[:mes].to_sym][nivel.to_sym] * 150.0

          tmp_kwh -= 150.0

          nivel = :intermedio_bajo
          total += TARIFA1C[params[:mes].to_sym][nivel.to_sym] * tmp_kwh

          return total
        end

        if kkwwhh <= 450.0
          nivel = :basico
          total += TARIFA1C[params[:mes].to_sym][nivel.to_sym] * 150.0

          tmp_kwh -= 150.0

          nivel = :intermedio_bajo
          total += TARIFA1C[params[:mes].to_sym][nivel.to_sym] * 150.0

          tmp_kwh -= 150.0

          nivel = :intermedio_alto
          total += TARIFA1C[params[:mes].to_sym][nivel.to_sym] * tmp_kwh

          return total
        end

        if kkwwhh > 450.0
          nivel = :basico
          total += TARIFA1C[params[:mes].to_sym][nivel.to_sym] * 150.0

          tmp_kwh -= 150.0

          nivel = :intermedio_bajo
          total += TARIFA1C[params[:mes].to_sym][nivel.to_sym] * 150.0

          tmp_kwh -= 150.0

          nivel = :intermedio_alto
          total += TARIFA1C[params[:mes].to_sym][nivel.to_sym] * 150.0

          tmp_kwh -= 150.0

          nivel = :excedente
          total += TARIFA1C[params[:mes].to_sym][nivel.to_sym] * tmp_kwh
          return total
        end
      end
    end
  end

  def calcular_agua params
    m3_actual = params[:metros_cubicos_actual].to_i
    m3_anterior = params[:metros_cubicos_anterior].to_i
    m3 = m3_actual - m3_anterior
    comision = 0

    if m3 > 200
      m3 = 200
    end

    if params[:tarifa] == 'tarifa1'
      if m3 >= 0 && m3 <= 6
        comision = 32.02
      elsif m3 >= 7 && m3 <= 10
        comision = 38.10
      else
        comision = 44.03
      end

      total = AGUA1[m3]
    else
      if m3 >= 0 && m3 <= 6
        comision = 45.76
      elsif m3 >= 7 && m3 <= 10
        comision = 54.44
      else
        comision = 62.93
      end

      total = AGUA2[m3]
    end

    return total + comision
  end

  def calcular_gas params
    consumo = params[:metros_cubicos_actual].to_i - params[:metros_cubicos_anterior].to_i
    distribucion = 4.381
    servicio = 58.94

    uso = GASTARIFA[params[:mes].to_sym] * consumo
    adquisision = consumo * distribucion

    return uso + adquisision + servicio
  end

  def send_mail
    UserMailer.with(user: self.user,recibo: self.bill_type, dia: self.payday).aviso_pago.deliver_now
  end
end
