class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  after_create :create_bills, :send_welcome_mail

  has_many :bills

  def create_bills
    Bill.create(bill_type: 0, user_id: self.id)
    Bill.create(bill_type: 1, user_id: self.id)
    Bill.create(bill_type: 2, user_id: self.id)
  end

  def send_welcome_mail
    UserMailer.with(user: self).welcome_email.deliver_now
  end
end
