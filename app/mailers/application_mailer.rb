class ApplicationMailer < ActionMailer::Base
  default from: 'mailer.rictv@gmail.com'
  layout 'mailer'
end
