class UserMailer < ApplicationMailer
  default from: 'mailer.rictv@gmail.com'

  def welcome_email
    @user = params[:user]
    @url  = 'https://simulatugasto.herokuapp.com/users/sign_in'
    mail(to: @user.email, subject: 'Bienvenido a simula tu gasto')
  end

  def aviso_pago
    @user = params[:user]
    @recibo = params[:recibo]
    @dia = params[:dia]
    mail(to: @user.email, subject: 'Su dia de corte se acerca')
  end

  def send_report
    @user = params[:user]
    @bill = params[:bill]
    @inputs = params[:inputs]
    mail(to: @user.email, subject: 'Reporte')
  end
end
