class BillsController < ApplicationController
  before_action :authenticate_user!

  def index
    @bills = current_user.bills
  end

  def show
    @bill = Bill.find(params[:id])

    unless @bill.user.id == current_user.id
      flash[:alert] = 'Acceso no permitido'
      return redirect_to bills_path
    end
  end

  def new
  end

  def create
  end

  def update
    @bill = Bill.find(params[:id])
    
    if @bill.update(bill_params)
      redirect_to @bill, notice: 'El recibo ha sido actiualizado.'
    else
      render :show
    end
  end

  def destroy
  end

  def calcular
    @bill = Bill.find(params[:bill_id])
  end

  def resolve
    @bill = Bill.find(params[:bill_id])
    @inputs = ''

    case @bill.bill_type
    when 'luz'
      luz = calc_luz_params
      if luz[:kwh_anterior].to_i > luz[:kwh_actual].to_i
        redirect_to bill_calcular_path,
          alert: 'La lectura anterior tiene que ser menor a la actual'
        return
      end
      @resultado = @bill.calcular_luz(calc_luz_params)
      @inputs += "<strong>Tarifa:</strong> #{calc_luz_params[:tarifa]}<br/>"
      @inputs += "<strong>Mes:</strong> #{calc_luz_params[:mes]}<br/>"
      @inputs += "<strong>kwh anterior:</strong> #{calc_luz_params[:kwh_anterior]}<br/>"
      @inputs += "<strong>kwh actual:</strong> #{calc_luz_params[:kwh_actual]}"
    when 'agua'
      agua = calc_agua_params
      if agua[:metros_cubicos_anterior].to_i > agua[:metros_cubicos_actual].to_i
        redirect_to bill_calcular_path,
          alert: 'La lectura anterior tiene que ser menor a la actual'
        return
      end
      @resultado = @bill.calcular_agua(calc_agua_params)
      @inputs += "<strong>Tarifa:</strong> #{calc_agua_params[:tarifa]}<br/>"
      @inputs += "<strong>Metros cubicos anteriores:</strong> #{calc_agua_params[:metros_cubicos_anterior]}<br/>"
      @inputs += "<strong>Metros cubicos actuales:</strong> #{calc_agua_params[:metros_cubicos_actual]}"

    when 'gas'
      gas = calc_gas_params
      if gas[:metros_cubicos_anterior].to_i > gas[:metros_cubicos_actual].to_i
        redirect_to bill_calcular_path,
          alert: 'La lectura anterior tiene que ser menor a la actual'
        return
      end
      @resultado = @bill.calcular_gas(calc_gas_params)
      @inputs += "<strong>Mes:</strong> #{calc_gas_params[:mes]}<br/>"
      @inputs += "<strong>Metros cubicos anteriores:</strong> #{calc_gas_params[:metros_cubicos_anterior]}<br/>"
      @inputs += "<strong>Metros cubicos actuales:</strong> #{calc_gas_params[:metros_cubicos_actual]}"
    end
    @bill.bill_payments.create(payment: @resultado,
                               date: Time.now,
                               inputs: @inputs)
  end

  def send_mail
    @bill = Bill.find(params[:bill_id])

    UserMailer.with(user: @bill.user,
                    bill: @bill,
                    inputs: @bill.bill_payments.last.inputs).send_report.deliver_now
  end

  private

  def bill_params
    params.require(:bill).permit(:payday)
  end

  def calc_luz_params
    params.require(:bill).permit(:mes, :tarifa, :kwh_anterior, :kwh_actual)
  end

  def calc_agua_params
    params.require(:bill).permit(:tarifa, :metros_cubicos_anterior, :metros_cubicos_actual)
  end

  def calc_gas_params
    params.require(:bill).permit(:mes, :metros_cubicos_anterior, :metros_cubicos_actual)
  end
end
