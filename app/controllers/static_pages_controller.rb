class StaticPagesController < ApplicationController
  def home
    if user_signed_in?
      redirect_to bills_path
    end
  end

  def app
    unless user_signed_in?
      redirect_to root_path
    end

    @bills = current_user.bills
  end
end
