Rails.application.routes.draw do
  devise_for :users
  root 'static_pages#home'

  get :app, to: 'static_pages#app'

  resources :bills do
    get :calcular, to: 'bills#calcular'
    get :send_mail, to: 'bills#send_mail'
    post :resolve, to: 'bills#resolve'
  end
end
