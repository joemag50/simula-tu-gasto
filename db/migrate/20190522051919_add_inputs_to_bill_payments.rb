class AddInputsToBillPayments < ActiveRecord::Migration[5.2]
  def change
    add_column :bill_payments, :inputs, :string
  end
end
