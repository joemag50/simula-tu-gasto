class CreateBillPayments < ActiveRecord::Migration[5.2]
  def change
    create_table :bill_payments do |t|
      t.numeric :payment
      t.timestamp :date
      t.string :periodo

      t.timestamps
    end
  end
end
