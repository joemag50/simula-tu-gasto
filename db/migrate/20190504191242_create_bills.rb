class CreateBills < ActiveRecord::Migration[5.2]
  def change
    create_table :bills do |t|
      t.integer :type
      t.timestamp :paydate
      t.integer :period

      t.timestamps
    end
  end
end
