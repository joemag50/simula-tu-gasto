class AddBillIdToBillPayments < ActiveRecord::Migration[5.2]
  def change
    add_column :bill_payments, :bill_id, :integer
  end
end
