class ChangeTypeDateBill < ActiveRecord::Migration[5.2]
  def change
    change_column :bills, :paydate, :datetime
  end
end
