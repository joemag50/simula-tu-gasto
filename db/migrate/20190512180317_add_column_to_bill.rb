class AddColumnToBill < ActiveRecord::Migration[5.2]
  def change
    add_column :bills, :payday, :integer
  end
end
