class RemoveColumnPayDateToBill < ActiveRecord::Migration[5.2]
  def change
    remove_column :bills, :paydate, :datetime
  end
end
